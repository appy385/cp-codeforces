#include<bits/stdc++.h>
using namespace std;
#define lli long long int
lli isSubstring(string s1, string s2)
{
    lli M = s1.length();
    lli N = s2.length();
    for (lli i = 0; i <= N - M; i++) {
        lli j;
        for (j = 0; j < M; j++)
            if (s2[i + j] != s1[j])
                break;
        if (j == M)
            return i;
    }

    return -1;
}
int main(){
  lli n;
  cin>>n;
  vector<pair<lli,string>>v;
  vector<string>v1;
  for(lli i=0;i<n;i++){
    string s;
    cin>>s;
    v.push_back(make_pair(s.length(),s));
  }
  sort(v.begin(),v.end());
  for(lli i=0;i<n;i++){
    v1.push_back(v[i].second);
  }
  for(lli i=0;i<n;i++){
    for(lli j=i+1;j<n;j++){
      if(isSubstring(v1[i],v1[j])==-1){
        cout<<"NO";
        return 0;
      }
    }
  }
  cout<<"YES"<<endl;
  for(lli i=0;i<v1.size();i++){
    cout<<v1[i]<<endl;
  }

}
