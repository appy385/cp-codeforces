#include<bits/stdc++.h>
using namespace std;
const int M=1e9+7;

long long int factorial(long long int n)
{
    if(n > 1)
        return n * factorial(n - 1);
    else
        return 1;
}
int main(){
  long long int t;
  cin>>t;
  while(t--){
    long long int n;
    cin>>n;
    int s;
    map<long long int,long long int>m;
    map <long long int,long long int> :: iterator itr;
    for(int i=1;i<=n;i++){
      cin>>s;
      m[s]++;
    }
    long long int ans=1;
    long long int flag=0;
    for (itr = m.begin(); itr != m.end(); ++itr)
   {
             long long int count= itr->second;
             if(flag){
               ans=(ans*count)%M;
              count--;
              flag=0;
             }
      if(count%2!=0){
        ans=(ans*count)%M;
         count=count-1;
         flag=1;
      }
      while(count>=2){
        long long int p=(factorial(count)/(factorial(count-2)*2))%M;
         ans=(ans*p)%M;
         count=count-2;
       }

   }
   cout<<ans<<endl;
  }
  return 0;
}
