#include<bits/stdc++.h>
using namespace std;
#define lli long long int
int main(){
  lli n, k;
  cin>>n>>k;
  if(k>50){
    cout<<"No";
  }
  else{
    map<lli,lli>m;
    vector<lli>v;
    for(lli i=1;i<=k;i++){
      if(m[n%i]<1){
        v.push_back(i);
      }
      m[n%i]+=1;
    }
    if(v.size()==k){
      cout<<"Yes"<<endl;
    }
    else{
      cout<<"No"<<endl;
    }
  }
  return 0;
}
