// Write your code h
#include<bits/stdc++.h>
using namespace std;
int main(){
    int t;
    scanf("%d",&t);
    while(t--){
        int n,m;
        scanf("%d %d",&n,&m);
        long long int sum=0;
        int a[m]={0};
        while(n--){
            long long int min=10000000;
            long long int c;
            for(int i=0;i<m;i++){
                scanf("%lld",&c);
                if(c<min && a[i]==0 ){
                    min=c;
                    a[i]=1;
                }
            }
            sum+=min;
        }
        printf("%lld\n",sum);
    }
    return 0;
}
