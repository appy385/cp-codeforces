#include<bits/stdc++.h>
using namespace std;
#define lli long long int
#define INF 1000000
#define N 1000010
vector<lli>v;
lli ans=INF;
lli a,b,k;
lli arr[N+1];
map<lli,lli>m;
bool Ver(lli cv)
{
    for(lli i = a; i <= b - cv + 1; i++)
        if(arr[i + cv - 1] - arr[i - 1] < k)
            return 0;
    return 1;
}

void SieveOfEratosthenes(int n)
{
    bool prime[n+1];
    memset(prime, true, sizeof(prime));
    for (lli p=2; p*p<=n; p++)
    {
        if (prime[p] == true)
        {
            for (lli i=p*2; i<=n; i += p)
                prime[i] = false;
        }
    }
    // Print all prime numbers
    for (lli p=2; p<=n; p++)
       if (prime[p]){
          v.push_back(p);
          m[p]=1;
        }
}
int main(){
  cin>>a>>b>>k;
  SieveOfEratosthenes(N);
  for(lli i = 2; i <N; i++) {
    if(m[i]==1){
      arr[i] = 1;
    }
    else{
      arr[i]=0;
    }
  }
  for(lli i=1;i<=N;i++){
    arr[i]+=arr[i-1];
  }
  lli P = 1, U = b - a + 1, M;
  while(P <= U)
  {
      M = (P + U) / 2;
      if(Ver(M)) ans = M, U = M- 1;
      else P = M + 1;
  }
  if(ans == INF)
      cout<< -1 << '\n';
  else
      cout<< ans << '\n';
}
