#include<bits/stdc++.h>
using namespace std;
vector<long long> tprime;
bool isprime(int n){
  for(int j=3;j*j<=n;j++){
      if(n%j==0) return false;
  }
  return true;
}
int binarysearch(int l,int r,long long val){
  if(l>r) return -1;
  int mid=(l+r)/2;
  if(tprime[mid]==val) return 0;
  else if(tprime[mid]>val) return binarysearch(l,mid-1,val);
  else return binarysearch(mid+1,r,val);

}
int main(){
  int n;
  long long a;
  cin>>n;

  tprime.push_back(4);
  for(int i=3;i<=1000000;i+=2){
      if(isprime(i)) tprime.push_back((long long )i*i);
  }
  while(n--){
    cin>>a;
    // puts(find (tprime.begin(), tprime.end(), a)!=tprime.end()?"YES":"NO"); //find is linear function
    puts(binarysearch(0,tprime.size()-1,a)==0?"YES":"NO");
  }

  return 0;
}
