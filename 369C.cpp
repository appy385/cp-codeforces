#include <iostream>
#include <cstdio>
#include <cstring>
#include <queue>
#include <vector>
#include <map>
#include <set>
#include <algorithm>
#define lowbit(x) ((x) & (-(x)))
using namespace std;
typedef long long LL;
const int N = 100005;
int n , dp[N],weight[N];
vector <int > edges[N];
vector<int> ans;
void dfs (int u , int p) {
      // dp[u]=weight[u];
      // cout<<u<<" "<<dp[u]<<endl;

    for (int i = 0 ; i < edges[u].size() ; i ++) {
        int v = edges[u][i] ;
        if (v == p) continue;
        dfs (v , u);
        if (weight[v] == 2) {
            if (dp[v] == 0) {
                ans.push_back(v);
            }
            dp[u] += max (1 , dp[v]);
        }
        else dp[u] += dp[v];
        // cout<<u<<" "<<dp[u]<<endl;

    }
}
int main () {
    cin >> n;
    for (int i = 1 ; i < n ; i ++) {
        int u , v , w;
        cin >> u >> v >> w;

        edges[u].push_back(v);
        edges[v].push_back(u);
        if(w==2){
        weight[u]=w;
        weight[v]=w;
      }
        // cout<<w<<endl;

    }
    dfs (1 , 0);
    // for(int i=1;i<=n;i++){
    //   if(weight[i] && dp[i]==1)
    //       ans.push_back(i);
    // }
    cout<<ans.size()<<endl;
    for(int i=0;i<ans.size();i++){
      cout<<ans[i]<<endl;
    }
    return 0;
}
