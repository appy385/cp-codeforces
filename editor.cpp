#include <iostream>
#include <cstdio>
#define f cin
#define g cout
#define NM 1000010
#define INF 0x3f3f3f3f

using namespace std;

int Ciur[NM], ANS = INF;
int  A, B, K;

bool Ver(int cv)
{
    for(int i = A; i <= B - cv + 1; i++)
        if(Ciur[i + cv - 1] - Ciur[i - 1] < K)
            return 0;
    return 1;
}

int i, j;

int main()
{
    for(i = 2; i < NM; i++) Ciur[i] = 1;
    for(i = 2; i < NM; i++)
        if(Ciur[i])
            for(j = i+i; j < NM; j += i)
                Ciur[j] = 0;

    for(i = 2; i < NM; i++)
        Ciur[i] += Ciur[i - 1];

    cin>> A >> B >> K;
    int P = 1, U = B - A + 1, M;

    while(P <= U)
    {
        M = (P + U) / 2;
        if(Ver(M)) ANS = M, U = M- 1;
        else P = M + 1;
    }

    if(ANS == INF)
        cout<< -1 << '\n';
    else
        cout<< ANS << '\n';

    return 0;
}
