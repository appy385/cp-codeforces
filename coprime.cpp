#include<bits/stdc++.h>
using namespace std;
int gcd(int u, int v)
{
    return (v != 0) ? gcd(v, u % v) : u;
}
int main(){
  int t;
  cin>>t;
  vector<int> p_prime;
  bool prime[51];
   memset(prime, true, sizeof(prime));

   for (int p=2; p*p<=50; p++)
   {
       // If prime[p] is not changed, then it is a prime
       if (prime[p] == true)
       {
           // Update all multiples of p
           for (int i=p*2; i<=50; i += p)
               prime[i] = false;
       }
   }

   // Print all prime numbers
   for (int p=2; p<=50; p++)
      if (prime[p])
         p_prime.push_back(p);
  while(t--){
    int c;
    int n;
    cin>>n;
    if(n==1) {
      cin>>c;
      cout<<0<<endl;
      cout<<c;
      continue;
    }
    vector<int>weight;
    for(int i=0;i<n;i++){
      cin>>c;
      // cout<<c<<endl;
      weight.push_back(c);
      // cout<<weight[i]<<endl;
    }
    int degree[n]={0};
    // vector<int>edges[n];
    // map<int,int>primec;

    for(int i=0;i<n;i++){
      // if(prime[weight[i]]) primec[weight[i]]=1;
      for(int j=i+1;j<n;j++){
        if(gcd(weight[i],weight[j])!=1) continue;
        degree[i]++;
        degree[j]++;
      }
    }
    // int count=0;
    int ans=0;
    int i=0,j=0;
    for(i=0;i<n;i++){
      if(degree[i]==0){
        for(j=0;j<p_prime.size();j++){
          int flag=0;
          if(gcd(weight[i],p_prime[j])==1){
              for(int k=0;k<n;k++){
                if(gcd(weight[k],p_prime[j])==1) continue;
                flag=1;
                break;

              }
              if(flag) continue;
              weight[i]=p_prime[j];
              ans=1;
              break;
          }
        }
        if(ans) break;

      }
    }
    cout<<ans<<endl;
    for(int j=0;j<n;j++){
      cout<<weight[j]<<" ";
    }
  }
  cout<<endl;

  return 0;
}
