#include<bits/stdc++.h>
#include <ctype.h>
using namespace std;
int main(){
  map<char,int>board;
  board['q']=9;
  board['r']=5;
  board['b']=3;
  board['n']=3;
  board['p']=1;
  board['k']=0;
  string s;
  int n=8;
  int white=0,black=0;
  while(n--){
    cin>>s;
    for(int i=0;i<s.size();i++){
      if(s[i]=='.')continue;
      if(isupper(s[i])){
        char c=tolower(s[i]);
        white+=board[c];
      }
      else   black+=board[s[i]];
    }
    // cout<<white<<" "<<black<<endl;
  }
  if(black==white) cout<<"Draw"<<endl;
  else if(black>white) cout<<"Black"<<endl;
  else cout<<"White"<<endl;


  return 0;
}
