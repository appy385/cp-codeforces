#include<bits/stdc++.h>
#include<ctype.h>

using namespace std;

int main(){
  string s1;
  cin>>s1;
  string ans;
  int size = s1.size();
  for(int i = 0; i<size;i++){
    if(s1[i]=='A' || s1[i]=='O'|| s1[i]== 'Y' || s1[i]=='E' || s1[i]=='U' || s1[i] == 'I' ) continue;
    ans+='.';
    ans+=tolower(s1[i]);
  }
  cout<<ans<<endl;
  return 0;
}
