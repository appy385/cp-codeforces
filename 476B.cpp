#include<bits/stdc++.h>
using namespace std;
int dp[11][11];
int main(){
  string dr;
  cin>>dr;
  string dm;
  cin>>dm;
  int drsum=0,dmsum=0,qcount=0;
  for(int i=0;i<dr.size();i++){
      if(dr[i]=='+') drsum++;
      else if(dr[i]=='-')drsum--;
  }
  for(int i=0;i<dm.size();i++){
      if(dm[i]=='+') dmsum++;
      else if(dm[i]=='-')dmsum--;
      else qcount++;
  }
  int diff=abs(drsum-dmsum);

  if(diff==0 && qcount==0){
    cout<<1.0<<endl;
    return 0;
  }
  if(diff>qcount || (qcount+diff)%2!=0){
     cout<<0.0<<endl;
     return 0;
  }
  dp[1][0]=0;dp[1][1]=1;
  for(int i=2;i<=qcount;i++){
    dp[i][0]=2*dp[i-1][1];
    for(int j=1;j<=i;j++){
      dp[i][j] = dp[i-1][j-1];
      dp[i][j]+=(j<i)?dp[i-1][j+1]:0 ;
    }
  }
  double ans=dp[qcount][diff]*1.0/(1<<qcount);
  printf("%.12f\n", ans) ;  //set precision value to 12

  return 0;
}
