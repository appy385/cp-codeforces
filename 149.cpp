#include<bits/stdc++.h>
using namespace std;
int getdigit(char c){
  if(c>='0' && c<='9') return c-'0';
  return c-'A'+10;

}
int convert(string s,int base){
  int sum=0;
  for(int i=0;i<s.size();i++){
    int digit=getdigit(s[i]);
    if(digit>=base) return 90;
    sum = sum*base +digit;
  }
  return sum;
}
int main(){
    string s;
    cin>>s;
    vector<int> v;

  int pos=s.find(":");
  string b=s.substr(pos+1);
  string a=s.substr(0,pos);
  for(int i=2;i<=60;i++){
    if(convert(a,i)<24 && convert(b,i)<60)
      v.push_back(i);
  }
  if(v.size()==0)
    cout<<"0"<<endl;
  else if(v.back()==60)
    cout<<"-1"<<endl;
  else{
    for(int i=0;i<v.size();i++)
      cout<<v[i]<<" ";
    cout<<"\n";
  }


  return 0;
}
