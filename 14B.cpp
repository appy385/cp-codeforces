#include<bits/stdc++.h>
using namespace std;
void swap(int *a,int *b){
  int t=*a;
  *a=*b;
  *b=t;
  return ;
}
int main(){
  int n,x0;
  int a,b,maxa=-15000,maxb=+15000;
  cin>>n>>x0;
  for(int i=0;i<n;i++){
    cin>>a>>b;
    if(a>b)
      swap(&a,&b);
    if(maxa<a)
      maxa=a;
    if(maxb>b)
      maxb=b;
  }
  if(maxa>maxb)
    cout<<"-1"<<endl;
  else if(maxa>x0)
    cout<<maxa-x0<<endl;
  else if(x0>maxb)
  cout<<x0-maxb<<endl;
  else
  cout<<"0"<<endl;

  return 0;
}
