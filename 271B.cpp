#include<bits/stdc++.h>
using namespace std;
#define lli long long int
vector<lli>v;
void SieveOfEratosthenes(int n)
{
    bool prime[n+1];
    memset(prime, true, sizeof(prime));

    for (lli p=2; p*p<=n; p++)
    {
        if (prime[p] == true)
        {
            for (lli i=p*2; i<=n; i += p)
                prime[i] = false;
        }
    }
    // Print all prime numbers
    for (lli p=2; p<=n; p++)
       if (prime[p]){
          v.push_back(p);
        }
}
int main(){
  lli n,m,sum=0;
  cin>>n>>m;
  vector<lli>v3;
  lli a[n][m];
  lli b[n][m];
  for(lli i=0;i<n;i++){
    for(lli j=0;j<m;j++){
      cin>>a[i][j];
    }
  }
  SieveOfEratosthenes(1000000);
  for(lli i=0;i<n;i++){
    for(lli j=0;j<m;j++){
      vector<lli>::iterator low;
      low=lower_bound (v.begin(), v.end(), a[i][j]);
      b[i][j]=v[low-v.begin()]-a[i][j];
    }
  }
  for(lli i=0;i<n;i++){
    sum=0;
    for(lli j=0;j<m;j++){
      sum+=b[i][j];
    }
    v3.push_back(sum);
  }
  for(lli i=0;i<m;i++){
    sum=0;
    for(lli j=0;j<n;j++){
      sum+=b[j][i];
    }
    v3.push_back(sum);
  }
  // for(lli i=0;i<11;i++){
  //   cout<<v[i]<<" ";
  // }
  // cout<<endl;
  sort(v3.begin(),v3.end());
  // for(lli i=0;i<n;i++){
  //   for(lli j=0;j<m;j++){
  //     cout<<b[i][j]<<" ";
  //   }
  //   cout<<endl;
  // }
  cout<<v3[0];
}
