#include<bits/stdc++.h>
using namespace std;
bool sortbysec(const pair<int,int> &a,
              const pair<int,int> &b)
{
    return (a.second < b.second)||(a.second==b.second && a.first<b.first) ;
}

int main(){
int n;
long long int p;
cin>>n;
vector<pair<int,long long int>> vect;
int a[(n+1)];
for(int i=1;i<=n;i++){
  cin>>p;
  vect.push_back(make_pair(i,p));
}
sort(vect.begin(),vect.end(),sortbysec);
a[vect[0].first]=-1;
int max=vect[0].first;
for(int i=1;i<n;i++){
  if(max>vect[i].first)
    a[vect[i].first]=(max-(vect[i].first+1));
  else{
    a[vect[i].first]=-1;
    max=vect[i].first;
  }
}
for(int i=1;i<=n;i++){
  cout<<a[i]<<" ";
}
cout<<endl;

return 0;
}
