#include<bits/stdc++.h>
using namespace std;
int main(){
  int n,x,k;
  cin>>n>>k;
  map<int,int>m;
  vector<int>v;
  for(int i=0;i<n;i++){
    cin>>x;
    if(m[x]<1){
      v.push_back(i+1);
    }
    m[x]+=1;
  }
  if(v.size()<k){
    cout<<"NO"<<endl;
  }
  else{
    cout<<"YES"<<endl;
    for(int i=0;i<k;i++){
      cout<<v[i]<<" ";
    }
  }
}
