#include<bits/stdc++.h>
using namespace std;
int main(){
  string s;
  cin>>s;
  long long hash1,hash2,hash;
  int n=s.size();
  if(n==1 || n==2) {
    cout<<"Just a Legend"<<endl;
    return 0;
  }
  const int p=31;
  const int m= 1e9 + 9;
  vector<long long>power(n);
  vector<long long>h1(n+1,0);
  vector<long long>h2(n+1,0);

  power[0]=1;
  for(int i=1;i<n;i++){
    power[i]=(power[i-1]*p)%m;
  }
  for(int i=0;i<n;i++){
    h1[i+1]=(h1[i]+(s[i]-'a'+1)*power[i])%m;
    h2[i+1]=(h2[i]*p+(s[n-i-1]-'a'+1))%m;
  }
  long long ans=0;
  for( int l=1;l<=(n-1);l++){
    // cout<<l<<" "<<h1[l]<<" "<<h2[l]<<endl;
    // cout<<"hash2value1= "<<hash2<<endl
    if(h1[l]!=h2[l]) continue;
    hash1=(h1[l]*power[n-1])%m;
    for(int i=1;i<=n-l-1;i++){
      hash=(abs((h1[i+l]+m-h1[i]))*power[n-i-1])%m;
      cout<<l<<" "<<hash<<" "<<hash1<<endl;

      if(hash==hash1){ ans=l; break;}
    }
  }
  if(ans==0) cout<<"Just a legend"<<endl;
  else cout<<s.substr(0,ans)<<endl;
  return 0;

}
