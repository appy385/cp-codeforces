#include<bits/stdc++.h>
#include<ctype.h>

using namespace std;

int main(){
  string s;
  cin>>s;
  string s1;
  for(char c: s){
    s1+=tolower(c);
  }
  string ans;
  int size = s1.size();
  for(int i = 0; i<size;i++){
    if(s1[i]=='a' || s1[i]=='e'|| s1[i]== 'i' || s1[i]=='o' || s1[i]=='u' || s1[i]=='y') continue;
    ans+='.';
    ans+=s1[i];
  }
  cout<<ans<<endl;
  return 0;
}
