#include<stdio.h>
#include<iostream>
#include<string>
#include<cstring>
#include <iomanip>
#include<stack>
#include<set>
#include<vector>
#include<algorithm>
#include<set>
#include<bitset>
#include<map>
#include<queue>
#include<math.h>
using namespace std;
#define mdlo 1000000007
#define maxERR 0.00000064
//#define minVal -10000000000000
#define maxBits 40
#define pi 3.1415926535897932384626433832795
#define INF 999999999999
typedef long long ll;
long long mInv[41];
long long fact[41];
long long factInv[41],M;
typedef pair<long,long>edge;
typedef vector<long>adjList;
long levels[15];
long nextPoint[15];
long firstDigit[15];
bool inTree[1];
bool visited[1];
struct treeInfo{
    long depth;
    long longestPath;
    long startPointOflongestpath;
    long deepestNode;
    bool included;
};
adjList tree[16];
vector<long>startPoints;
//long finalCount[1000106];
adjList graph[15];
long long getmoduloInv(long long n){
    if(n==1)
        return 1;
    if(mInv[n]>0)
        return mInv[n];
    long long m=(-1*mdlo)/n;
    m+=mdlo;
    m*=getmoduloInv(mdlo%n);
    mInv[n]=(m%mdlo);
    return mInv[n];
}
ll par[45];
ll getAncestor(ll val){
    if(par[val]!=val)
        par[val]=getAncestor(par[val]);
    return par[val];
}
bool unify(ll a,ll b){
    ll para=getAncestor(a),parb=getAncestor(b);
    if(para==parb)
        return false;
    if(para<parb)
        par[parb]=para;
    else
        par[para]=parb;
    return true;
}
vector<unsigned long> primes;
vector<unsigned long> get_primes(unsigned long max){

    char *sieve;
    sieve = new char[max/8+1];
    // Fill sieve with 1
    long m=(max/8)+1;
    for(long long i=0;i<m;i++)
        sieve[i]=255;
    for(unsigned long x = 2; x <= max; x++)
        if(sieve[x/8] & (0x01 << (x % 8))){
            primes.push_back(x);
            // Is prime. Mark multiplicates.
            for(unsigned long j = 2*x; j <= max; j += x)
                sieve[j/8] &= ~(0x01 << (j % 8));
        }
    delete[] sieve;
    return primes;
}
double dp[2][2],p;
long nextRight[2012];
long nextLeft[2012];
ll treepos[2012];
ll H,N;
long f[100005],b[100005],posOf[100005],a[100005];
double height,pll[2012],plr[2012],prl[2012],prr[2012];
double getAvarageOverLap(long pos,bool fallForward,bool next){
    if((pos==0&&next==false)||(pos==N&&next))
    return 0;
    long neighbour,maxDist,curDist,overlap;
    if(next)
        neighbour=pos+1;
    else
        neighbour=pos-1;
    curDist=abs(treepos[pos]-treepos[neighbour]);
    if(fallForward!=next){
        maxDist=H;
    }
    else
        maxDist=2*H;
    if(maxDist<=curDist)
        return 0;
    overlap=maxDist-curDist;
    double probabilty;
    //s=pll^n+((pll-pll^(n+1))/(1-pll))

    if(next)
        probabilty=1.0-(prr[nextRight[neighbour]+1-neighbour]+((prr[1]-prr[nextRight[neighbour]+2-neighbour])/(1-prr[1])));
    else
        probabilty=1.0-(pll[1+neighbour-nextLeft[neighbour]]+((pll[1]-pll[(2+neighbour-nextLeft[neighbour])])/(1-pll[1])));
    return (double)overlap*probabilty;

}
double getGround(long startPoint, long endPoint){
    if(startPoint>endPoint)
        return 0;
    if(dp[startPoint][endPoint]<0.0){
        double groundLL,groundLR,groundRR,groundRL;
        if(startPoint==endPoint){
            dp[startPoint][endPoint]=height;
            return dp[startPoint][endPoint];
        }
        groundLL=height+getGround(startPoint+1,endPoint)-getAvarageOverLap(startPoint,false,true);
        groundLR=height+treepos[min(nextRight[startPoint],endPoint)]-treepos[startPoint]+getGround(nextRight[startPoint]+1,endPoint);
        groundRL=height+treepos[endPoint]-treepos[max(startPoint,nextLeft[endPoint])]+getGround(startPoint,nextLeft[endPoint]-1);
        groundRR=getGround(startPoint,endPoint-1)+height-getAvarageOverLap(endPoint,true,false);
        if(nextRight[startPoint]<endPoint){
            groundLR=-getAvarageOverLap(nextRight[startPoint],true,true);
            groundRL-=getAvarageOverLap(nextLeft[endPoint],false,false);
        }
        dp[startPoint][endPoint]=(pll[1]*groundLL)+(prr[1]*groundLR)+(pll[1]*groundRL)+(prr[1]*groundRR);
    }
    return dp[startPoint][endPoint];
}
//s=pll^n+((pll-pll^(n+1))/(1-pll))
double getdp(long startPoint,long endPoint){
    if(startPoint>endPoint)
        return 0;
    if(dp[startPoint][endPoint]==0){

    }
    return dp[startPoint][endPoint];
}
bool isPrime(long N){
    if(N==1)
    return false;
    if(N<4)
        return true;
    for(long i=2;i*i<N;i++){
        if(N%i==0)
            return false;
    }
    return true;
}
ll gcd(ll a,ll b){
    ll c;
    if(a>b){
        c=a;
        a=b;
        b=c;
    }
    if(a==0)
        return b;
    c=b%a;
    while(c>0){
        b=a;
        a=c;
        c=b%a;
    }
    return a;
}
long long getLcm(long long a,long long b){
    long long c=a/gcd(a,b);
    return b*c;
}
treeInfo dfs(long startPoint){
    treeInfo tInfo;
    long maxDepth=0,maxDepth2=0,deepestNode=1234567,deepestNode2=1234567,longestPath=0,longestPathStart=1234567;
    bool status=inTree[startPoint];
    visited[startPoint]=true;
    for(long i=0;i<tree[startPoint].size();i++){
        if(!visited[tree[startPoint][i]]){
            treeInfo childInfo=dfs(tree[startPoint][i]);
            if(childInfo.included){
                status=true;
                if(maxDepth<childInfo.depth||(maxDepth==childInfo.depth&&deepestNode>childInfo.deepestNode)){
                    maxDepth2=maxDepth;
                    deepestNode2=deepestNode;
                    deepestNode=childInfo.deepestNode;
                    maxDepth=childInfo.depth;
                }
                else if(maxDepth2<childInfo.depth||(maxDepth2==childInfo.depth&&deepestNode2>childInfo.deepestNode)){
                    maxDepth2=childInfo.depth;
                    deepestNode2=childInfo.deepestNode;
                }
                if(childInfo.longestPath>longestPath||(childInfo.longestPath==longestPath&&longestPathStart<childInfo.startPointOflongestpath)){
                    longestPath=childInfo.longestPath;
                    longestPathStart=childInfo.startPointOflongestpath;
                }
            }
        }
    }
    tInfo.included=status;
    if(maxDepth==0){
        tInfo.deepestNode=startPoint;
        tInfo.depth=1;
        tInfo.longestPath=1;
        tInfo.startPointOflongestpath=startPoint;
        return tInfo;
    }
    if(maxDepth+maxDepth2+1>longestPath||(maxDepth+maxDepth2+1==longestPath&&longestPathStart>min(deepestNode,deepestNode2))){
        longestPathStart=min(deepestNode,deepestNode2);
        longestPath=maxDepth+maxDepth2+1;
    }
    if(maxDepth2==maxDepth&&deepestNode2<deepestNode)
        deepestNode=deepestNode2;
    tInfo.deepestNode=deepestNode;
    tInfo.depth=maxDepth+1;
    tInfo.longestPath=longestPath;
    tInfo.startPointOflongestpath=longestPathStart;
    inTree[startPoint]=status;
    return tInfo;
}
ll getVal(ll num){
    ll ans=1;
    for(long i=1;i<num;i++)
        ans+=(num/gcd(num,i));
    return ans;
}
void buildDp(){
    memset(dp,0,sizeof(dp));
    for(long i=1;i<22;i++){
        for(long j=1;j<=i;j++){
            dp[i][j]=getdp(i,j);
            cout<<dp[i][j]<<" ";
        }
        cout<<endl;
    }
}
double angle[1];

int main(void){
//  freopen ("E:/inputSmall.txt","r",stdin);
 //   freopen ("E:/outputSmall2.txt","w",stdout);
    ll test_cases=1;//,C,D,A,L,R,rep,round=0,rem,ans=0,num;
    memset(posOf,-1,sizeof(posOf));
  //double st,en,minerr=0.00000001,r,v;
  //cin>>test_cases>>r>>v;
    cout << setprecision(15) << fixed;
 // buildpwr2(20);
 ll d1,d2,d3,ans1,ans2,n,m;
    for(long T=0;T<test_cases;T++){
        cin>>n>>m;
        for(long i=0;i<n;i++){
            cin>>f[i];
            if(posOf[f[i]]==-1)
                posOf[f[i]]=i;
            else
                posOf[f[i]]=-2;
        }
        bool possible=true,ambigous=false;
        for(long i=0;i<m;i++){
            cin>>b[i];
            if(posOf[b[i]]==-1){
                possible=false;
            }
            else if(posOf[b[i]]==-2)
                ambigous=true;
            else a[i]=posOf[b[i]]+1;
        }
        if(!possible)
            cout<<"Impossible\n";
        else if(ambigous)
            cout<<"Ambiguity\n";
        else{
            cout<<"Possible\n";
            for(long i=0;i<m;i++)
                cout<<a[i]<<" ";
        }

        //cout<<min(ans1,ans2)<<endl;
            //cout<<(low+high)/2<<endl;
//      memset(finalCount,0,sizeof(finalCount));


     //   cout<<steps<<endl;
    }
    return 0;
}
/*ll cumDist1[100005];
ll cumDist2[100005];
vector<ll>distFrom1;
vector<ll>distFrom2;
void initFact(){
    fact[0]=1;
    factInv[0]=1;
    for(ll i=1;i<0;i++){
        factInv[i]=(factInv[i-1]*getmoduloInv(i))%mdlo;
        fact[i]=(fact[i-1]*i)%mdlo;
    }
}
bool isLeapyear(long year){
    if(year%4==0&&(year%100!=0||year%400==0))
        return true;
    return false;
}
ll ds,ms,ys,de,me,ye,dc,mc,yc,day;
void next13th(){
    if(ds<13){
        ds=13;
        return;
    }
    ms++;
    ds=13;
    if(ms>12)
    {
        ms=1;
        ys++;
    }
}
    long months[]={31,28,31,30,31,30,31,31,30,31,30,31};
    long dayBeforeMonth[12];
bool validDate(){
    if(ys>ye)
        return false;
    if(ys<ye)
        return true;
    if(ms>me)
        return false;
    if(ms<me)
        return true;
    if(ds>de)
        return false;
    return true;
}
long getDay(long d,long m,long y){
    long day=((y-1900)*365+(y-1900)/4-(y-1900)/100+(y-1600)/400+dayBeforeMonth[m-1]+d+2)%7;
    if(y%4==0&&m<3){
        if(y%400==0||y%100!=0)
            day=(day+6)%7;
    }
    return day;
}
bool iscurDayFriday(){
    if(getDay(ds,ms,ys)==0)
        return true;
    return false;
}
void dijkstra(long startPoint){
    long m;
    if(startPoint==1)
        m=0;
    else
        m=1;
    distMatrix[m][startPoint]=0;
    priority_queue<edge, vector<edge>,greater <edge>>pq;
    pq.push(edge(0,startPoint));
    while(!pq.empty()){
        edge top=pq.top();
        pq.pop();
        ll d= top.first,u=top.second;
        if(distMatrix[m][u]==d){
            for(long i=0;i<graph[u].size();i++){
                ll v=graph[u][i].second;
                ll ec=graph[u][i].first;
                if(distMatrix[m][v]>distMatrix[m][u]+ec){
                    distMatrix[m][v]=distMatrix[m][u]+ec;
                    pq.push(edge(distMatrix[m][v],v));
                }
            }
        }
    }
}
void buildSumSegmentTree(long long b,long long e,long long node){
    if(b==e)
    {
        segTree[node]=seqlens[b];
         return;
    }
    long long mid=(b+e)/2;
    buildSumSegmentTree(b,mid,node*2);
    buildSumSegmentTree(mid+1,e,1+(node*2));
    segTree[node]=max(segTree[2*node],segTree[1+(node*2)]);
}
ll getMaxlen(ll b, ll e, ll node, ll qb, ll qe){
    if(b>qe||e<qb)
        return 0;
    if(b>=qb&&e<=qe)
        return segTree[node];
    ll m=(b+e)/2;
    ll f=getMaxlen(b,m,node*2,qb,qe),s=getMaxlen(m+1,e,1+(node*2),qb,qe);
    return max(f,s);
}
ll vals[1];
ll dp[1];
ll totalSum[1];
long long mInv[1];
long long fact[1];
long long factInv[1];

//long long pow10[18];
long long getmoduloInv(long long n){
    if(n==1)
        return 1;
    if(mInv[n]>0)
        return mInv[n];
    long long m=(-1*mdlo)/n;
    m+=mdlo;
    m*=getmoduloInv(mdlo%n);
    mInv[n]=(m%mdlo);
    return mInv[n];
}
long long onNthbit(long pos,long val){
    return val|pwr2[pos];
}
bool isNthbitOn(long long pos,long long val){
    return (val&pwr2[pos])>0;
}

seg segTree[5];
long N;
void buildSumSegmentTree(long long b,long long e,long long node){
    if(b==e)
    {
         segTree[node].first=vals[b];
         return;
    }
    long long mid=(b+e)/2;
    buildSumSegmentTree(b,mid,node*2);
    buildSumSegmentTree(mid+1,e,1+(node*2));
    segTree[node].first=segTree[2*node].first+segTree[1+(node*2)].first;
}
long long getSum(long long qStart,long long qEnd,long long sStart,long long sEnd,long long node,prop p){

    long k=sEnd-sStart,n;
    for(long i=0;i<p.size();i++){
        segTree[node].second.push_back(p[i]);
        n=p[i];
        segTree[node].first+=(((n+k)*(n+k+1)*(2*n+2*k+1)-(n*(n-1)*(2*n-1)))/6+((n+k)*(n+k+1)-n*(n-1)))/2;
        }
    if(qEnd<sStart||qStart>sEnd)
    {
        return 0;
    }
    if(sStart>=qStart&&sEnd<=qEnd)
    {
         return segTree[node].first;
    }
    long long sMid=(sStart+sEnd)/2;
    prop l;
    for(long i=0;segTree[node].second.size();i++)
        l.push_back(segTree[node].second[i]+sMid+1-sStart);
    return getSum(qStart,qEnd,sStart,sMid,2*node,segTree[node].second)+getSum(qStart,qEnd,sMid+1,sEnd,1+(2*node),l);
}
long long update(ll node, long long sStart,ll sEnd,long upStart,long upEnd){
    if(upEnd<sStart||upStart>sEnd)
        return 0;
    long k=sEnd-sStart,n=sStart+1-upStart;
    if(upStart<=sStart&&upEnd>=sEnd){
        segTree[node].second.push_back(n);
        segTree[node].first+=(((n+k)*(n+k+1)*(2*n+2*k+1))/6);
        segTree[node].first-=(((n*(n-1)*(2*n-1)))/6);
        segTree[node].first+=(((n+k)*(n+k+1)-n*(n-1)))/2;
        return 0;
    }
    ll sMid=(sStart+sEnd)/2;
    update(node*2,sStart,sMid,upStart,upEnd);
    update(1+(node*2),sMid+1,sEnd,upStart,upEnd);
    segTree[node].first=segTree[2*node].first+segTree[1+(node*2)].first;

    for(long i=0;i<segTree[node].second.size();i++){
        n=segTree[node].second[i];
        segTree[node].first+=(((n+k)*(n+k+1)*(2*n+2*k+1)-(n*(n-1)*(2*n-1)))/6+((n+k)*(n+k+1)-n*(n-1)))/2;
    }
    return segTree[node].first;
}
void update(long x,long y){
    long len=y-x+1,add=0;
    for(long i=0;i<len;i++){
        add+=(i+1)*2;
        update(1,0,N-1,x+i,add+vals[x+i]);
    }
}
long getlen(long long num){
    long len=0;
    while(num>0){
        num/=10;
        len++;
    }
    return len;
}
encryptedLine lineConverter(string s){
    encryptedLine el;
    ll val=0,len=s.size();
    for(long i=0;i<len;i++){
        if(s[i]==' '){
            el.push_back(val);
            val=0;
        }
        else{
            val*=26;
            val+=(s[i]-'a');
        }
    }
    el.push_back(val);
    return el;
}
long getPalinDrome(long num){
    long a=num%10;
    long b=(num/10)%10;
    long c=(num/100)%10;
    return num*1000+a*100+b*10+c;
}

vector<unsigned long> get_primes(unsigned long max){
    vector<unsigned long> primes;
    char *sieve;
    sieve = new char[max/8+1];
    // Fill sieve with 1
    long m=(max/8)+1;
    for(long long i=0;i<m;i++)
        sieve[i]=255;
    for(unsigned long x = 2; x <= max; x++)
        if(sieve[x/8] & (0x01 << (x % 8))){
            primes.push_back(x);
            // Is prime. Mark multiplicates.
            for(unsigned long j = 2*x; j <= max; j += x)
                sieve[j/8] &= ~(0x01 << (j % 8));
        }
    delete[] sieve;
    return primes;
}string add(string a,string b){
    int l1=a.size();
    int l2=b.size();
    while(l1<l2){
        a="0"+a;
        l1++;
    }
    while(l2<l1){
        b="0"+b;
        l2++;
    }
    string s;
    int carry=0;
    for(long i=l2-1;i>-1;i--){
        int d=a[i]+b[i]+carry-2*'0';
        carry=d/10;
        a[i]='0'+(d%10);
    }
    if(carry)
        a="1"+a;
    return a;
}
long long mInv[1010];
long long fact[1000];
long long factInv[1000];
long long getmoduloInv(long long n){
    if(n==1)
        return 1;
    if(mInv[n]>0)
        return mInv[n];
    long long m=(-1*mdlo)/n;
    m+=mdlo;
    m*=getmoduloInv(mdlo%n);
    mInv[n]=(m%mdlo);
    return mInv[n];

}

long dp[200010];
vector<weightedPos>points;
vector<weightedPos>modifiedPositions;
vector<long long>weightPlusPos;
long long segTree[530010];
long N,X,W;
void buildSegTree(long node,long b,long e){
    if(b==e)
        segTree[node]=b;
    else{
        long mid=(b+e)/2;
        buildSegTree(node*2,b,mid);
        buildSegTree(1+node*2,mid+1,e);
        if(weightPlusPos[segTree[node*2]]<=weightPlusPos[segTree[1+node*2]])
            segTree[node]=segTree[node*2];
        else
            segTree[node]=segTree[1+node*2];
    }
}
long long getMin(long node,long segBeg,long segEnd,long qBeg,long qEnd){
    if(segEnd<qBeg||segBeg>qEnd)
        return N;
    if(segBeg>=qBeg&&segEnd<=qEnd)
        return segTree[node];
    long mid=(segBeg+segEnd)/2;
    long fh=getMin(node*2,segBeg,mid,qBeg,qEnd);
    long sh=getMin(1+node*2,mid+1,segEnd,qBeg,qEnd);
    if(weightPlusPos[sh]<weightPlusPos[fh])
        return sh;
    return fh;
}
long long getMinPos(long node,long segBeg,long segEnd,long qBeg,long qEnd){
    if(segEnd<qBeg||segBeg<qEnd)
        return 0;
    if(segBeg>=qBeg&&segEnd<=qEnd)
        return segTree[node];
    long mid=(segBeg+segEnd)/2;
    long fh=getMin(node*2,segBeg,mid,qBeg,qEnd);
    long sh=getMin(node*2,mid+1,segEnd,qBeg,qEnd);
    return min(fh,sh);
}
long getPos(long val){
    long low=0,high=modifiedPositions.size()-1;
    long mid=(low+high)/2;
    while(low<high){
        if(modifiedPositions[mid].first<val)
            low=mid+1;
        else
            high=mid-1;
        mid=(low+high)/2;
    }
    if(mid>0)
        mid--;
    while(mid<modifiedPositions.size()&&modifiedPositions[mid].first<val)
        mid++;
    return mid;
}
long getMaxGroup(long index){
    if(dp[index]>-1)
        return dp[index];
    long pos=getPos(points[index].first+points[index].second);
    if(pos>=N)
        return 1;
    long nextPoint=getMin(1,0,N-1,pos,N-1);
    dp[index]=1+getMaxGroup(nextPoint);
    return dp[index];
}
*/
