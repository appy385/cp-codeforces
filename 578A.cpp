#include<bits/stdc++.h>
using namespace std;
#define lli long long int
int main(){
  lli a, b;
  double ans=1e10;
  cin>>a>>b;
  if(a<b){
    cout<<-1;
    return 0;
  }
  lli x=(a-b)/b;
  if(x%2==1){
    x--;
  }
  if(x){
    ans=min(ans,(a-b)*1.0/x);
  }
  x=(a+b)/b;
  if(x%2==1){
    x--;
  }
  if(x){
    ans=min(ans,(a+b)*1.0/x);
  }
  cout<<setprecision(25)<<ans<<endl;
}
