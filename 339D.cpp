#include<bits/stdc++.h>
using namespace std;
int a[1<<17];
int tree[1<<18];
void build(int t,int start,int end,int node){
  if(start==end)
    tree[node]=a[start];
  else{
    int mid=(start+end)/2;
    build(t^1,start,mid,2*node);
    build(t^1,mid+1,end,2*node+1);
    if(!t)
      tree[node]=tree[2*node]^tree[2*node+1];
    else
      tree[node] =tree[2*node]|tree[2*node+1];
  }
}
void update(int t,int start,int end,int idx,int node){
   if(start==end)
      tree[node]=a[idx];
  else{
    int mid=(start+end)/2;
    if(start<=idx && idx<=mid)
      update(t^1,start,mid,idx,2*node);
    else
      update(t^1,mid+1,end,idx,2*node+1);

    if(!t)
        tree[node]=tree[2*node]^tree[2*node+1];
    else
        tree[node]=tree[2*node]|tree[2*node+1];
  }
}
int main(){
  int n,m,p,q;
  cin>>n>>m;

  for(int i=0;i<(1<<n);i++)
        cin>>a[i];
  build(n&1,0,(1<<n)-1,1);
  for(int i=0;i<m;i++){
      cin>>p>>q;
      p=p-1;
      a[p]=q;
      update(n&1,0,(1<<n)-1,p,1);
      cout<<tree[1]<<endl;
  }

  return 0;
}
