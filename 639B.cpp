#include<bits/stdc++.h>
using namespace std;
int main(){
  vector<pair<int,int> > edges;
  long int n,d,h;
  cin>>n>>d>>h;
  if(d-(2*h)>0){
    cout<<"-1"<<endl;
    return 0;
  }
  for(int i=0;i<h;i++){
    edges.push_back(make_pair(i,i+1));
  }
  if(h==d){
    if(h==1 && n!=2){
      cout<<"-1"<<endl;
      return 0;
    }
    else{
      for(int i=h+1;i<n;i++)
          edges.push_back(make_pair(1,i));
        }
  }
  else{
    edges.push_back(make_pair(0,h+1));
    for(int i=0;i<d-h-1;i++){
      edges.push_back(make_pair(i+h+1,i+h+2));
    }
    for(int i=d+1;i<n;i++){
      edges.push_back(make_pair(0,i));
    }
  }
  for(int i=0;i<edges.size();i++){
    cout<<edges[i].first + 1 <<" "<<edges[i].second +1<<"\n";
  }

  return 0;
}
