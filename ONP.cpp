#include<iostream>
#include<stack>
#include<string.h>
#include<map>
using namespace std;
int main(){
  int t;
  cin>>t;
  map<char,int> ex;
  ex['(']=0;
  ex[')']=0;
  ex['+']=1;
  ex['-']=2;
  ex['*']=3;
  ex['/']=4;
  ex['^']=5;

  while(t--){
    string s,ans;
    cin>>s;
    stack<char> k;
      for(int i=0;i<s.size();i++){
        if(s[i]=='(') k.push(s[i]);
        else if(s[i]==')'){
          while(k.top()!='('){
            ans+=k.top();
            k.pop();
          }
          k.pop();
        }

        else if(s[i]=='+' || s[i]=='-' || s[i]=='*' || s[i]=='/' || s[i]=='^'){
          char c=k.top();
            while(ex[c]>ex[s[i]]){
              ans+=c;
              k.pop();
              c=k.top();
            }
          k.push(s[i]);
        }
        else  ans+=s[i];
      }
      cout<<ans<<endl;
  }
  return 0;

}
