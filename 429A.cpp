#include<bits/stdc++.h>

using namespace std;
vector<int> v[n];
vector<vector<int> >edges(100000);
vector<int> init;
vector<int> goal;
int visited[100000];
vector<int> ans;
void dfs(int s,int even,int odd,int level){
  visited[s]=1;
  // cout<<s<<endl;
  if(level%2==0 && even%2!=0){
      init[s]=!init[s];
   }
  if(level%2!=0 && odd%2!=0)
        init[s]=!init[s];

  if(init[s]!=goal[s]){
    if(level%2==0) even++;
    else odd++;
    init[s]=!init[s];
    ans.push_back(s+1);
  }


  for(int i=0;i<edges[s].size();i++){
    if(!visited[edges[s][i]])
        dfs(edges[s][i],even,odd,level+1);
  }
  return;

}

int main(){
  int n;
  cin>>n;
  int u,v;
  for(int i=1;i<=n-1;i++){
    cin>>u>>v;
    u--;
    v--;
    edges[u].push_back(v);
    edges[v].push_back(u);
  }
  int in;
  for(int i=0;i<n;i++){
    cin>>in;
    init.push_back(in);
  }
  int go;
  for(int i=0;i<n;i++){
    cin>>go;
    goal.push_back(go);
  }
  dfs(0,0,0,0);
  cout<<ans.size()<<endl;
  for(int i=0;i<ans.size();i++){
    cout<<ans[i]<<endl;
  }

  return 0;
}
