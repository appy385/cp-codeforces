#include<bits/stdc++.h>
#define lli long long int
using namespace std;
// map<lli,lli>mp,mp1;
vector<lli>v;
int main(){
  lli n,m,ambig=0,impo=0,x;
  cin>>n>>m;
  lli b[m],f[n+1],mp[100101];
  // memset(mp,0,n);
  for(lli i=0;i<=100100;i++){
    mp[i]=0;
  }
  for(lli i=0;i<n;i++){
    cin>>f[i];
    if(mp[f[i]]==0){
      mp[f[i]]=i+1;
    }
    else{
      mp[f[i]]=-2;
    }
  }
  for(lli i=0;i<m;i++){
    cin>>b[i];
  }
  for(lli i=0;i<m;i++){
    if(mp[b[i]]==0){
      impo=1;
    }
    else if(mp[b[i]]==-2){
      ambig=1;
    }
    else{
      v.push_back(mp[b[i]]);
    }
  }
  if(impo==1){
    cout<<"Impossible"<<endl;
  }
  else if(ambig==1){
    cout<<"Ambiguity"<<endl;
  }
  else{
    cout<<"Possible"<<endl;
    for(lli i=0;i<v.size();i++){
      cout<<v[i]<<" ";
    }
  }
}
