#include<bits/stdc++.h>
using namespace std;
vector<vector<int> >vec(100000);
int visited[100000];
int degree[100000];
int leaf=0;
double sum=0.000000000000000;
void dfs(int s,double length){
  visited[s]=1;
  for(int i=0;i<vec[s].size();i++ && degree[s]){
    if(!visited[vec[s][i]])
      dfs(vec[s][i],length/degree[s]);
  }
  if(s)
    sum+=length;
    // cout<<length<<endl;


}
int main(){
  int n,u,v;
  cin>>n;
  for(int i=1;i<=n-1;i++){
    cin>>u>>v;
    u--;
    v--;
    vec[u].push_back(v);
    vec[v].push_back(u);
    degree[u]++;
    degree[v]++;
  }
  for(int i=1;i<n;i++)
    degree[i]--;
  dfs(0,1.000000000000000);
  // cout<<leaf<<endl;
  // cout<<sum<<endl;
  cout.precision(15);
  cout<<sum<<endl;
return 0;
}
