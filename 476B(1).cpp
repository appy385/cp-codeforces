#include<bits/stdc++.h>
using namespace std;
int dp[11][11];
int main(){
  string dr;
  cin>>dr;
  string dm;
  cin>>dm;
  int drsum=0,dmsum=0,qcount=0;
  for(int i=0;i<dr.size();i++){
      if(dr[i]=='+') drsum++;
      else if(dr[i]=='-')drsum--;
  }
  for(int i=0;i<dm.size();i++){
      if(dm[i]=='+') dmsum++;
      else if(dm[i]=='-')dmsum--;
      else qcount++;
  }
  int diff=abs(drsum-dmsum);

  if(diff==0 && qcount==0){
    cout<<1.0<<endl;
    return 0;
  }
  if(diff>qcount || (qcount+diff)%2!=0){
     cout<<0.0<<endl;
     return 0;
  }
  double ans=1;
  int d=( qcount+diff)/2;
  for(int i=0;i<d;i++){
    ans*=(qcount-i);
  }
  for(int i=2;i<=d;i++){
    ans/=i;
  }
  // cout<<ans<<endl;
  ans=(double)ans/(1<<qcount);
  printf("%.12f\n", ans) ;

  return 0;
}
